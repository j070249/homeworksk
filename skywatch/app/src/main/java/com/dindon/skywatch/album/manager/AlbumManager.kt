package com.dindon.skywatch.album.manager

import com.dindon.skywatch.album.model.AlbumModel
import com.dindon.skywatch.album.model.PhotoModel
import com.dindon.skywatch.album.model.UserModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request

//like Repository, you can fetch data from cloud or DB
object AlbumManager {

    private var _user_data = arrayListOf<UserModel>()
    private var _album_data = arrayListOf<AlbumModel>()
    private var _photo_data = arrayListOf<PhotoModel>()

    suspend fun getUserData(): ArrayList<UserModel>? {
        if (!_user_data.isEmpty())
            return _user_data
        else
            return withContext(Dispatchers.IO) {
                val client = OkHttpClient()
                val request = Request.Builder()
                    .url("https://jsonplaceholder.typicode.com/users")
                    .get()
                    .addHeader("Accept", "application/json")
                    .build()
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    var jsonData = response.body?.string()
                    val listType = object : TypeToken<ArrayList<UserModel>>() {}.type
                    val jsonArr = Gson().fromJson<ArrayList<UserModel>>(jsonData, listType)
                    _user_data = jsonArr
                    jsonArr
                } else {
                    null
                }
            }
    }

    suspend fun getAlbums(): ArrayList<AlbumModel>? {
        if (!_album_data.isEmpty())
            return _album_data
        else
            return withContext(Dispatchers.IO) {
                val client = OkHttpClient()
                val request = Request.Builder()
                    .url("https://jsonplaceholder.typicode.com/albums")
                    .get()
                    .addHeader("Accept", "application/json")
                    .build()
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    var jsonData = response.body?.string()
                    val listType = object : TypeToken<ArrayList<AlbumModel>>() {}.type
                    val jsonArr = Gson().fromJson<ArrayList<AlbumModel>>(jsonData, listType)
                    _album_data = jsonArr
                    jsonArr
                } else {
                    null
                }
            }
    }

    suspend fun getPhotos(): ArrayList<PhotoModel>? {
        if (!_photo_data.isEmpty())
            return _photo_data
        else
            return withContext(Dispatchers.IO) {
                val client = OkHttpClient()
                val request = Request.Builder()
                    .url("https://jsonplaceholder.typicode.com/photos")
                    .get()
                    .addHeader("Accept", "application/json")
                    .build()
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    var jsonData = response.body?.string()
                    val listType = object : TypeToken<ArrayList<PhotoModel>>() {}.type
                    val jsonArr = Gson().fromJson<ArrayList<PhotoModel>>(jsonData, listType)
                    _photo_data = jsonArr
                    jsonArr
                } else {
                    null
                }
            }
    }
}