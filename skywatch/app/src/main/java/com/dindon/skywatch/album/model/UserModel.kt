package com.dindon.skywatch.album.model

import com.google.gson.annotations.SerializedName

class UserModel {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("username")
    var username: String? = null
}