package com.dindon.skywatch.album.model

import com.google.gson.annotations.SerializedName

class AlbumModel {
    @SerializedName("userId")
    var userId: Int? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null
}