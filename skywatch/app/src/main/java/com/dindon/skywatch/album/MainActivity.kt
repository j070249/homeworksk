package com.dindon.skywatch.album

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import com.dindon.skywatch.BaseActivity
import com.dindon.skywatch.R
import com.dindon.skywatch.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,getLayoutId())
        changeFragment(AlbumListFragment::class.java)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun createViewModel(): ViewModel? {
        return null
    }
}