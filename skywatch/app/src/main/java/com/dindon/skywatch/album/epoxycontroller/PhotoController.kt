package com.dindon.skywatch.album.epoxycontroller

import com.airbnb.epoxy.EpoxyController
import com.dindon.skywatch.album.model.PhotoModel
import com.dindon.skywatch.photoContent


class PhotoController: EpoxyController() {

    var photoList: ArrayList<PhotoModel> = arrayListOf()
        set(value) {
            field = value
            requestModelBuild()
        }


    override fun buildModels() {
        photoList.forEachIndexed { index, data ->
            photoContent {
                id("photoList$index")
                title(data.title)
                imageUrl(data.url)
            }
        }
    }
}