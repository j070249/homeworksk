package com.dindon.skywatch.album.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dindon.skywatch.album.manager.AlbumManager
import com.dindon.skywatch.album.model.AlbumModel
import com.dindon.skywatch.album.model.PhotoModel
import com.dindon.skywatch.album.model.UserModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class AlbumListFragmentViewModel : ViewModel() {
    var userData = MutableLiveData<ArrayList<UserModel>>(arrayListOf())
    var albumData = MutableLiveData<ArrayList<AlbumModel>>(arrayListOf())
    var photoData = MutableLiveData<ArrayList<PhotoModel>>(arrayListOf())
    private var job: Job? = null

    open fun fetchData() {
        if (job == null)
            job = viewModelScope.launch {
                userData.postValue(AlbumManager.getUserData())
                albumData.postValue(AlbumManager.getAlbums())
                photoData.postValue(AlbumManager.getPhotos())
                job = null
            }
    }
}