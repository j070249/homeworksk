package com.dindon.skywatch.album.epoxycontroller

import com.airbnb.epoxy.EpoxyController
import com.dindon.skywatch.album.model.UserModel
import com.dindon.skywatch.albumUser


class UserController: EpoxyController() {

    var userList: ArrayList<UserModel> = arrayListOf()
        set(value) {
            field = value
            requestModelBuild()
        }

    var selected: Int = 0
        set(value) {
            field = value
            requestModelBuild()
        }

    var onEntryClick: ((UserModel?) -> Unit)? = null

    override fun buildModels() {

        userList.forEachIndexed { index, data ->
            albumUser {
                id("userList$index")
                user(data.username)
                selected(data.id == selected)
                clickEvent { _ -> onEntryClick?.invoke(data) }
            }
        }
    }
}