package com.dindon.skywatch.album

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dindon.skywatch.BaseActivity
import com.dindon.skywatch.BaseFragment
import com.dindon.skywatch.R
import com.dindon.skywatch.album.epoxycontroller.ContentController
import com.dindon.skywatch.album.epoxycontroller.UserController
import com.dindon.skywatch.album.model.AlbumModel
import com.dindon.skywatch.album.viewmodel.AlbumListFragmentViewModel
import com.dindon.skywatch.databinding.FragmentAlbumListBinding

class AlbumListFragment : BaseFragment() {

    lateinit var binding: FragmentAlbumListBinding
    lateinit var viewModel: AlbumListFragmentViewModel

    private lateinit var userController: UserController
    private lateinit var contentController: ContentController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, getLayoutId(),container,false)
        binding.setLifecycleOwner(this)
        viewModel = createViewModel()
        binding.vm = viewModel

        // init controller
        userController = UserController()
        contentController = ContentController()

        userController.onEntryClick = {
            val userId = it?.id
            userController.selected = userId!!

            // pass and filter album data to contentController
            var data = viewModel.albumData.value?.filter { it.userId == userId }
            contentController.albumList = data as ArrayList<AlbumModel>
        }
        contentController.onEntryClick = {
            val bundle = Bundle()
            bundle.putInt("ALBUM_ID", it?.id!!)
            (activity as BaseActivity).changeFragment(AlbumContentFragment::class.java, bundle)
        }

        // binding controller
        binding.epoxyRecyclerViewUser.setController(userController)
        binding.epoxyRecyclerViewContent.setController(contentController)

        //binging controller data
        viewModel.userData.observe(viewLifecycleOwner, Observer {
            userController.userList = it
        })
        viewModel.photoData.observe(viewLifecycleOwner, Observer {
            contentController.photoList = it
        })

        //fetch data
        viewModel.fetchData()
        return  binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_album_list
    }

    override fun createViewModel(): AlbumListFragmentViewModel {
        return ViewModelProvider(requireActivity()).get(AlbumListFragmentViewModel::class.java)
    }

}