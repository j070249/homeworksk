package com.dindon.skywatch

import com.airbnb.epoxy.EpoxyDataBindingPattern

@EpoxyDataBindingPattern(rClass = R::class, layoutPrefix = "epoxy_")
object EpoxyDataBindingPatterns
