package com.dindon.skywatch.album

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dindon.skywatch.BaseFragment
import com.dindon.skywatch.R
import com.dindon.skywatch.album.epoxycontroller.PhotoController
import com.dindon.skywatch.album.model.PhotoModel
import com.dindon.skywatch.album.viewmodel.AlbumContentFragmentViewModel
import com.dindon.skywatch.databinding.FragmentAlbumContentBinding

class AlbumContentFragment : BaseFragment() {

    lateinit var binding: FragmentAlbumContentBinding
    lateinit var viewModel: AlbumContentFragmentViewModel
    var albumId = 0

    private lateinit var controller: PhotoController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, getLayoutId(),container,false)
        binding.setLifecycleOwner(this)
        viewModel = createViewModel()
        binding.vm = viewModel
        albumId = arguments?.getInt("ALBUM_ID")!!

        // init controller
        controller = PhotoController()

        // binding controller
        binding.epoxyRecyclerView.setController(controller)

        //binging controller data
        viewModel.photoData.observe(viewLifecycleOwner, Observer {
            val data = it.filter { it.albumId == albumId }
            controller.photoList = data as ArrayList<PhotoModel>
        })

        //fetch data
        viewModel.fetchData()
        return  binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_album_content
    }

    override fun createViewModel(): AlbumContentFragmentViewModel {
        return ViewModelProvider(requireActivity()).get(AlbumContentFragmentViewModel::class.java)
    }

}