package com.dindon.skywatch.album.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dindon.skywatch.album.manager.AlbumManager
import com.dindon.skywatch.album.model.PhotoModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class AlbumContentFragmentViewModel : ViewModel() {
    var photoData = MutableLiveData<ArrayList<PhotoModel>>(arrayListOf())
    private var job: Job? = null


    open fun fetchData() {
        if (job == null)
            job = viewModelScope.launch {
                photoData.postValue(AlbumManager.getPhotos())
                job = null
            }
    }
}