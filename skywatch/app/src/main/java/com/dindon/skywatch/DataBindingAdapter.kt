package com.dindon.skywatch

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.target.Target


@BindingAdapter("imageUrl")
fun bindImage(imageView: ImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        val glideUrl = GlideUrl(url, LazyHeaders.Builder()
            .addHeader("User-Agent", "Mobile")
            .build())
        GlideApp.with(imageView.context)
            .load(glideUrl)
            .placeholder(R.drawable.bg_img)
            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(imageView)
    }
}