package com.dindon.skywatch

import android.os.Bundle
import android.util.Log
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            this.finish()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    protected open fun getLayoutId(): Int {
        return R.layout.activity_base
    }

    protected abstract fun createViewModel(): ViewModel?

    open fun changeFragment(
        fragClass: Class<out BaseFragment>,
        bundle: Bundle? = null
    ) {
        changeFragment(fragClass, bundle, R.id.content_fragment)
    }

    open fun changeFragment(
        fragClass: Class<out BaseFragment?>,
        bundle: Bundle? = null,
        @IdRes viewId: Int
    ) {
        val fragmentManager = this.supportFragmentManager
        var fragment = fragmentManager.findFragmentByTag(fragClass.canonicalName) as BaseFragment?

        if (null == fragment) {
            fragment = try {
                fragClass.newInstance()
            } catch (e: Exception) {
                e.printStackTrace()
                return
            }
        }

        if (bundle != null) {
            fragment?.arguments = bundle
        }

        fragmentManager.beginTransaction()
            .add(viewId, fragment!!, fragClass.canonicalName)
            .addToBackStack(fragClass.canonicalName)
            .commit()
    }
}