package com.dindon.skywatch.album.epoxycontroller

import com.airbnb.epoxy.EpoxyController
import com.dindon.skywatch.album.model.AlbumModel
import com.dindon.skywatch.album.model.PhotoModel
import com.dindon.skywatch.albumContent
import com.dindon.skywatch.albumContentType1


class ContentController: EpoxyController() {

    var albumList: ArrayList<AlbumModel> = arrayListOf()
        set(value) {
            field = value
            requestModelBuild()
        }

    var photoList: ArrayList<PhotoModel> = arrayListOf()
        set(value) {
            field = value
            requestModelBuild()
        }

    var onEntryClick: ((AlbumModel?) -> Unit)? = null

    override fun buildModels() {

        val _albumList = albumList.chunked(3)
        _albumList.forEachIndexed { index, data ->
            if (index == 0)
                albumContentType1 {
                    id("albumListType $index")
                    if (data.isNotEmpty()) {
                        imageUrl1(
                            photoList[((data[0]?.id?.minus(1))?.times(50) ?: 0) + 1].thumbnailUrl
                        )
                        clickEvent1 { _ -> onEntryClick?.invoke(data[0]) }
                    }
                    if (data.size > 1) {
                        imageUrl2(
                            photoList[((data[1]?.id?.minus(1))?.times(50) ?: 0) + 1].thumbnailUrl
                        )
                        clickEvent2 { _ -> onEntryClick?.invoke(data[1]) }
                    }
                    if (data.size > 2) {
                        imageUrl3(
                            photoList[((data[2]?.id?.minus(1))?.times(50) ?: 0) + 1].thumbnailUrl
                        )
                        clickEvent3 { _ -> onEntryClick?.invoke(data[2]) }
                    }
                }
            else
                albumContent {
                    id("albumListType $index")
                    if (data.isNotEmpty()) {
                        imageUrl1(
                            photoList[((data[0]?.id?.minus(1))?.times(50) ?: 0) + 1].thumbnailUrl
                        )
                        clickEvent1 { _ -> onEntryClick?.invoke(data[0]) }
                    }
                    if (data.size > 1) {
                        imageUrl2(
                            photoList[((data[1]?.id?.minus(1))?.times(50) ?: 0) + 1].thumbnailUrl
                        )
                        clickEvent2 { _ -> onEntryClick?.invoke(data[1]) }
                    }
                    if (data.size > 2) {
                        imageUrl3(
                            photoList[((data[2]?.id?.minus(1))?.times(50) ?: 0) + 1].thumbnailUrl
                        )
                        clickEvent3 { _ -> onEntryClick?.invoke(data[2]) }
                    }
                }
        }
    }
}